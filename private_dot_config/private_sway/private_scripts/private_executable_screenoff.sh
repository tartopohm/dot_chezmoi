#!/bin/sh

# https://github.com/swaywm/sway/issues/2910
swayidle timeout 1 'swaymsg "output eDP-1 power off"' \
    resume 'swaymsg "output eDP-1 power on "; pkill -nx swayidle'
