#!/usr/bin/sh

# Requires acpid, and that the service be started

send_val() {
    volume="$(pamixer --get-volume)"

    if [ "$volume" -gt 150 ]; then
        pamixer --set-volume 150 --allow-boost
    fi
}

send_val
acpi_listen | grep --line-buffered VOLUP | while read -r _ ; do
    sleep .1
    send_val
done
