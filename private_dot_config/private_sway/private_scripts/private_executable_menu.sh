#!/bin/sh

pkill bemenu || j4-dmenu-desktop \
    --dmenu="bemenu \
        --fixed-height \
        --ignorecase \
        --list=8 \
        --scrollbar=none \
        --fn='Inconsolata Bold 14' \
        --prompt='' \
        --wrap \
        --no-overlap \
        --fb=#1e2326e6 \
        --nb=#1e2326e6 \
        --af=#d3c6aa \
        --hb=#1e2326e6 \
        --hf=#7fbbb3 \
        --ab=#1e2326e6 \
        --nf=#d3c6aa \
        --tf=#d3c6aa \
        --scb=#00000000 \
        --tb=#1e2326e6 \
        --ff=#d3c6aa" \
    --no-generic \
    --term=alacritty | xargs swaymsg exec --

