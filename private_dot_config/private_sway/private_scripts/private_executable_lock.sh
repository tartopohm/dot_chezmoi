#!/bin/sh

eww update isPowerExt=false
swaylock --daemonize -K \
    -i $HOME/.config/sway/assets/arch_white_on_black.png -s center \
    -l --indicator-radius 230 --indicator-thickness 12 \
    --text-ver-color 00000000 --text-wrong-color 00000000 \
    --key-hl-color   b0b0b0   --inside-color 00000000
