#!/usr/bin/sh

step=$1
threshold=$2
volume="$(pamixer --get-volume)"

if [ `expr $volume + $step` -le $threshold ]; then
    pamixer -i $step --allow-boost
else
    pamixer --set-volume $threshold --allow-boost
fi
