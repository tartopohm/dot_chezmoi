#!/usr/bin/env lua

local skm = vim.keymap.set
local opts = { noremap = true, silent = true, nowait = true }

skm("n", "<leader>pv", vim.cmd.Ex)
skm("n", "<Space><Space>", ":nohlsearch<CR>", opts)

vim.api.nvim_create_autocmd("FileType", {
    group = vim.api.nvim_create_augroup("CloseWithQ", { clear = true }),
    pattern = {
        "checkhealth",
        "help",
        "man",
    },
    callback = function(event)
        vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf })
    end,
})

-- cycle between tags
-- skm("i", "<C-l>", [[<Cmd>lua require('neogen').jump_next()<CR>]], opts)
-- skm("i", "<C-h>", [[<Cmd>lua require('neogen').jump_prev()<CR>]], opts)

-- keep selection selected after indentation
skm("v", "<", "<gv", opts)
skm("v", ">", ">gv", opts)

-- diagnostics
skm("n", "<space>e", vim.diagnostic.open_float)
skm("n", "[d", vim.diagnostic.goto_prev)
skm("n", "]d", vim.diagnostic.goto_next)
skm("n", "<space>q", vim.diagnostic.setloclist)

------------
-- Aerial --
------------
skm("n", "<Leader>a", "<cmd>AerialToggle!<CR>", opts)
