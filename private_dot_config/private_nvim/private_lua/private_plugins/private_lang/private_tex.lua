return {
    {
        "lervag/vimtex",
        ft = { "tex", "bib" },
        config = function()
            -- PDF viewer
            vim.g.vimtex_view_method = "zathura"
            --- do not open viewer on compile
            -- vim.g.vimtex_view_automatic = false

            -- folds
            vim.g.vimtex_fold_enabled = true
            -- faster and more control
            vim.g.vimtex_fold_manual = true
            vim.g.vimtex_fold_types = {
                preamble = { enabled = false },
            }

            -- quickfix
            vim.g.vimtex_quickfix_open_on_warning = false

            -- conceal; concealcursor is set in main option file
            vim.api.nvim_command("set conceallevel=0")
            vim.g.vimtex_syntax_conceal = {
                math_super_sub = 0,
                math_fracs = 0,
            }

            -- latexmk options
            vim.g.vimtex_compiler_latexmk = {
                backend = "nvim",
                continuous = false,
                build_dir = "out",
            }

            vim.g.vimtex_delim_toggle_mod_list = {
                { [[\left]], [[\right]] },
                { [[\bigl]], [[\bigr]] },
                { [[\Bigl]], [[\Bigr]] },
                { [[\biggl]], [[\biggr]] },
                { [[\Biggl]], [[\Biggr]] },
            }

            -- disable spell checking in comments
            vim.g.vimtex_syntax_nospell_comments = 1

            vim.g.vimtex_toc_config = {
                name = "TOC",
                layers = { "content", "todo" },
                resize = 0,
                split_width = 50,
                todo_sorted = 1,
                show_help = 1,
                show_numbers = 1,
                mode = 1,
            }
        end,
    },
    -- {
    --     "anufrievroman/vim-angry-reviewer",
    --     ft = { "tex" },
    --     config = function()
    --         vim.g.AngryReviewerEnglish = "british"
    --         vim.keymap.set("n", "<leader>lr", "<cmd>AngryReviewer<cr>")
    --     end,
    -- },
}
