#!/usr/bin/env lua

return {
    -- Colorscheme
    "sainnhe/everforest",
    -- Context in status line
    -- Comments
    -- "tpope/vim-commentary",

    -- Context indentation line
    -- use {
    --     'lukas-reineke/indent-blankline.nvim',
    --     config = function()
    --         require("config._blankline").setup()
    --     end,
    --     ft = {"python", "tex", "vim", "lua"},
    -- }
}
