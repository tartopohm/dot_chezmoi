return {
    {
        "folke/trouble.nvim",
        opts = {
            modes = {
                diag_errors = {
                    mode = "diagnostics", -- inherit from diagnostics mode
                    filter = {
                        any = {
                            buf = 0, -- current buffer
                            {
                                severity = vim.diagnostic.severity.ERROR, -- errors only
                                -- limit to files in the current project
                                function(item)
                                    return item.filename:find((vim.loop or vim.uv).cwd(), 1, true)
                                end,
                            },
                        },
                    },
                },
                diag_preview_s = {
                    mode = "diagnostics",
                    preview = {
                        type = "split",
                        relative = "win",
                        position = "right",
                        size = 0.3,
                    },
                },
            },
        },
        dependencies = "nvim-tree/nvim-web-devicons",
        cmd = "Trouble",
        keys = {
            {
                "<leader>xx",
                "<cmd>Trouble diagnostics toggle<cr>",
                desc = "Diagnostics (Trouble)",
            },
            {
                "<leader>xX",
                "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
                desc = "Buffer Diagnostics (Trouble)",
            },
            {
                "<leader>cs",
                "<cmd>Trouble symbols toggle focus=false<cr>",
                desc = "Symbols (Trouble)",
            },
            {
                "<leader>cl",
                "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
                desc = "LSP Definitions / references / ... (Trouble)",
            },
        },
    },
    {
        "folke/todo-comments.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        opts = {},
        cmd = { "TodoLocList", "TodoTelescope" },
        keys = {
            {
                "<leader>tt",
                "<cmd>TodoLocList<cr>",
                desc = "Location List (Todo Comments)",
            },
            {
                "<leader>ft",
                "<cmd>TodoTelescope<cr>",
                desc = "Search through project TODOs with Telescope",
            },
            {
                "<leader>xt",
                "<cmd>Trouble todo<cr>",
                desc = "Project TODOs (Trouble)",
            },
        },
    },
}
