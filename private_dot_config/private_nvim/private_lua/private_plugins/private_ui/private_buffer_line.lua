return {
    "willothy/nvim-cokeline",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-tree/nvim-web-devicons",
    },
    config = function()
        local str_rep = string.rep

        local min_buffer_width = 21

        -- local bg_dim = "#1e2326"
        -- local bg0 = "#272e33"
        local bg1 = "#2e383c"
        local bg3 = "#414b50"
        local gray1 = "#859289"
        local gray2 = "#9da9a0"
        local fg = "#D3C6AA"
        local red = "#e67e80"
        -- local green = vim.g.terminal_color_2
        -- local yellow = vim.g.terminal_color_3

        local function background(buffer)
            if buffer.is_focused then
                return bg3
            else
                if buffer.is_hovered then
                    return bg1
                else
                    return "Normal"
                end
            end
            -- return (buffer.is_focused and bg3) or (buffer.is_hovered and bg1 or "Normal")
        end

        local function foreground(buffer)
            return buffer.is_focused and fg or gray1
        end

        local _components = {
            separator = {
                text = " ",
                bg = "Normal",
                truncation = { priority = 1 },
            },
            devicon = {
                text = function(buffer)
                    return " " .. buffer.devicon.icon .. " "
                end,
                bg = background,
                fg = function(buffer)
                    return buffer.is_focused and buffer.devicon.color or foreground(buffer)
                end,
                truncation = { priority = 1 },
            },
            unique_prefix = {
                text = function(buffer)
                    return buffer.unique_prefix
                end,
                bg = background,
                fg = function(buffer)
                    return buffer.is_focused and gray2 or foreground(buffer)
                end,
                truncation = {
                    priority = 3,
                    direction = "left",
                },
            },
            filename = {
                text = function(buffer)
                    return buffer.filename
                end,
                bg = background,
                fg = foreground,
                italic = function(buffer)
                    return buffer.is_modified
                end,
                truncation = {
                    priority = 2,
                    direction = "left",
                },
            },
            diagnostics = {
                text = function(buffer)
                    return (buffer.diagnostics.errors ~= 0 and "  ")
                        or (buffer.diagnostics.warnings ~= 0 and "  ")
                        or ""
                end,
                bg = background,
                fg = function(buffer)
                    return (buffer.diagnostics.errors ~= 0 and "DiagnosticError")
                        or (buffer.diagnostics.warnings ~= 0 and "DiagnosticWarn")
                        or foreground(buffer)
                end,
            },
            close_or_unsaved = {
                text = " 󰅗 ",
                bg = background,
                fg = function(buffer)
                    return buffer.is_hovered and red or foreground(buffer)
                end,
                delete_buffer_on_left_click = true,
                truncation = { priority = 1 },
            },
        }

        local get_remaining_space = function(buffer)
            local used_space = 0
            for _, component in pairs(_components) do
                used_space = used_space
                    + vim.fn.strwidth(
                        (type(component.text) == "string" and component.text)
                            or (type(component.text) == "function" and component.text(buffer))
                    )
            end
            return math.max(0, min_buffer_width - used_space)
        end

        local padding = {
            text = function(buffer)
                return str_rep(" ", get_remaining_space(buffer))
            end,
            bg = background,
        }

        require("cokeline").setup({
            -- Only show the bufferline when there are at least this many visible buffers.
            -- -- default: `1`.
            show_if_buffers_are_at_least = 1,

            buffers = {
                -- If set to `last` new buffers are added to the end of the bufferline,
                -- if `next` they are added next to the current buffer.
                -- default: 'last',
                new_buffers_position = "last",
            },
            rendering = {
                max_buffer_width = min_buffer_width,
            },
            mappings = {
                -- Controls what happens when the first (last) buffer is focused and you
                -- try to focus/switch the previous (next) buffer. If `true` the last
                -- (first) buffers gets focused/switched, if `false` nothing happens.
                -- default: `true`.
                cycle_prev_next = false,
            },
            components = {
                _components.separator,
                -- _components.space,
                -- _components.left_half_circle,
                -- left_padding,
                _components.devicon,
                -- _components.number,
                _components.unique_prefix,
                _components.filename,
                -- _components.space,
                -- right_padding,
                padding,
                _components.diagnostics,
                -- _components.space,
                _components.close_or_unsaved,
                -- _components.space,
                -- _components.right_half_circle,
            },
        })

        local vks = vim.keymap.set
        -- Focus the previous/next buffer
        vks("n", "<S-Tab>", "<Plug>(cokeline-focus-prev)")
        vks("n", "<Tab>", "<Plug>(cokeline-focus-next)")

        -- Switch the position of the current buffer with the previous/next buffer.
        vks("n", "<Leader>cp", "<Plug>(cokeline-switch-prev)")
        vks("n", "<Leader>cn", "<Plug>(cokeline-switch-next)")
    end,
}
