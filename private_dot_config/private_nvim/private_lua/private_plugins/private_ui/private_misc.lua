return {
    "inside/vim-search-pulse",
    "RRethy/vim-illuminate",
    {
        "stevearc/aerial.nvim",
        opts = {},
        -- Optional dependencies
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "nvim-tree/nvim-web-devicons",
        },
    },
    {
        "lewis6991/foldsigns.nvim",
        opts = {},
        event = "VeryLazy",
    },
}
