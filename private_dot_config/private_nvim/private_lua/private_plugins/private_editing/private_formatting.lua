return {
    "stevearc/conform.nvim",
    opts = {},
    config = function()
        -- stevearc/conform.nvim/blob/master/doc/recipes.md
        -- #command-to-toggle-format-on-save
        vim.api.nvim_create_user_command("ConformDisable", function(args)
            if args.bang then
                -- FormatDisable! will disable formatting just for this buffer

                vim.b.disable_autoformat = true
            else
                vim.g.disable_autoformat = true
            end
        end, {
            desc = "Disable autoformat-on-save",
            bang = true,
        })
        vim.api.nvim_create_user_command("ConformEnable", function()
            vim.b.disable_autoformat = false
            vim.g.disable_autoformat = false
        end, {
            desc = "Re-enable autoformat-on-save",
        })

        require("conform").setup({
            formatters_by_ft = {
                lua = { "stylua" },
                python = { "black" },
            },
            format_after_save = function(bufnr)
                -- Disable with a global or buffer-local variable
                if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
                    return
                end
                return {
                    lsp_fallback = true,
                }
            end,
        })
    end,
}
