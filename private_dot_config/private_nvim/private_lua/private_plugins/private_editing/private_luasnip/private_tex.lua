local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local d = ls.dynamic_node
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local rep = require("luasnip.extras").rep

return {
    -- Example of a multiline text node
    s({ trig = "lines", dscr = "Demo: a text node with three lines." }, {
        t({ "Line 1", "Line 2", "Line 3" }),
    }),
    s({ trig = ";a", snippetType = "autosnippet" }, { t([[\alpha]]) }),
    s({ trig = ";ff", dscr = "Expands into '\frac{}{}'", snippetType = "autosnippet" }, {
        t("\\frac{"),
        i(1), -- insert node 1
        t("}{"),
        i(2), -- insert node 2
        t("}"),
    }),
    s({ trig = ",,", snippetType = "autosnippet" }, { t([[\]]) }),
    -- Code for environment snippet in the above GIF
    s(
        { trig = ";ee", snippetType = "autosnippet" },
        fmta(
            [[
                \begin{<>}
                    <>
                \end{<>}
            ]],
            {
                i(1, "env"),
                i(2, "body"),
                rep(1), -- this node repeats insert node i(1)
            }
        )
    ),
}
