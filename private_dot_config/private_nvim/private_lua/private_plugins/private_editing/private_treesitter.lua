return {
    "nvim-treesitter/nvim-treesitter",
    ft = { "css", "latex", "lua", "python", "ruby", "scss", "vim", "yuck" },
    build = ":TSUpdate",
    config = function()
        require("nvim-treesitter.configs").setup({
            ensure_installed = {
                "css",
                "latex",
                "lua",
                "luadoc",
                "python",
                "ruby",
                "scss",
                "vim",
                "yuck",
            },
            sync_install = false,
            ignore_install = {},
            highlight = {
                enable = true,
                disable = { "latex" },
                additional_vim_regex_highlighting = false,
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = "gnn",
                    node_incremental = "grn",
                    scope_incremental = "grc",
                    node_decremental = "grm",
                },
            },
            -- pyfold = {
            --     enable = false,
            --     custom_foldtext = true,
            -- },
        })
    end,
}
