return {
    "mfussenegger/nvim-lint",
    config = function()
        local lint = require("lint")
        lint.linters_by_ft = {
            lua = { "selene" },
            python = { "flake8" },
            tex = { "chktex" },
        }
        vim.api.nvim_create_autocmd({ "BufWinEnter", "BufWritePost", "InsertLeave" }, {
            callback = function()
                lint.try_lint()
            end,
        })
    end,
    ft = {
        "lua",
        "python",
        "tex",
    },
}
