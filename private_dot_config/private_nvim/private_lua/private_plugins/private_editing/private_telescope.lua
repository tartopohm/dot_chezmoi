return {
    "nvim-telescope/telescope.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
        local telescope = require("telescope")

        telescope.setup({
            extensions = {
                aerial = {
                    -- Display symbols as <root>.<parent>.<symbol>
                    show_nesting = { ["_"] = true },
                },
            },
        })
        telescope.load_extension("aerial")

        local builtin = require("telescope.builtin")
        local vks = vim.keymap.set
        vks("n", "<leader>pf", builtin.find_files)
        vks("n", "<C-p>", builtin.git_files)
        vks("n", "<leader>pg", builtin.live_grep)
        vks("n", "<leader>fb", builtin.buffers)
        vks("n", "<leader>fh", builtin.help_tags)
        vks("n", "<leader>fa", [[<Cmd>Telescope aerial<CR>]])
    end,
}
