return {
    "ThePrimeagen/refactoring.nvim",
    dependencies = {
        { "nvim-lua/plenary.nvim" },
        { "nvim-treesitter/nvim-treesitter" },
    },
    ft = { "python", "lua", "ruby", "c", "cpp" },
    config = function()
        local vks = vim.keymap.set

        vks("x", "<leader>re", ":Refactor extract ")
        vks("x", "<leader>rf", ":Refactor extract_to_file ")
        vks("x", "<leader>rv", ":Refactor extract_var ")
        vks("n", "<leader>rb", ":Refactor extract_block")
        vks("n", "<leader>rbf", ":Refactor extract_block_to_file")
        vks("n", "<leader>rI", ":Refactor inline_func")
        vks({ "n", "x" }, "<leader>ri", ":Refactor inline_var")

        vks("n", "<leader>rp", function()
            require("refactoring").debug.printf({ below = false })
        end)
        vks({ "x", "n" }, "<leader>rpv", function()
            require("refactoring").debug.print_var()
        end)
        vks("n", "<leader>rc", function()
            require("refactoring").debug.cleanup({})
        end)
    end,
}
