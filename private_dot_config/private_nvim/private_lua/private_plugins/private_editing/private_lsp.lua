return {
    {
        "neovim/nvim-lspconfig",
        event = { "BufReadPre", "BufNewFile" },
        dependencies = {
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "VonHeikemen/lsp-zero.nvim",
            "hrsh7th/cmp-nvim-lsp",
            "folke/neodev.nvim",
        },
        config = function()
            require("neodev").setup({
                override = function(root_dir, library)
                    if root_dir:find("chezmoi") then
                        library.enabled = true
                        library.plugins = true
                    end
                end,
            })
            local lsp_zero = require("lsp-zero")
            local lsp_config = require("lspconfig")

            lsp_zero.on_attach(function(_, bufnr)
                lsp_zero.default_keymaps({ buffer = bufnr })
            end)

            local function ltex(use)
                if use then
                    local function read_file(file)
                        local dictionary = {}
                        for line in io.lines(file) do
                            table.insert(dictionary, line)
                        end
                        return dictionary
                    end

                    lsp_config.ltex.setup({
                        settings = {
                            ltex = {
                                checkFrequency = "save",
                                language = "en-NZ",
                                additionalRules = {
                                    motherTongue = "fr",
                                    enablePickyRules = true,
                                },
                                dictionary = {
                                    ["en-NZ"] = read_file(
                                        vim.fn.stdpath("config") .. "/spell/en.utf-8.add"
                                    ),
                                },
                                latex = {
                                    commands = {
                                        ["\\includeonly{}"] = "ignore",
                                    },
                                },
                            },
                        },
                    })
                else
                    return lsp_zero.noop
                end
            end

            require("mason").setup({})
            require("mason-lspconfig").setup({
                handlers = {
                    lsp_zero.default_setup,
                    ltex = ltex(false),
                    lsp_config.yamlls.setup({
                        settings = {
                            yaml = {
                                schemaStore = {
                                    -- You must disable built-in schemaStore support if you want to use
                                    -- this plugin and its advanced options like `ignore`.
                                    enable = false,
                                    -- Avoid TypeError: Cannot read properties of undefined (reading 'length')
                                    url = "",
                                },
                                schemas = require("schemastore").yaml.schemas({
                                    select = { "Mason Registry" },
                                }),
                            },
                        },
                    }),
                },
            })
        end,
    },
    {
        "williamboman/mason.nvim",
        cmd = "Mason",
    },
}
