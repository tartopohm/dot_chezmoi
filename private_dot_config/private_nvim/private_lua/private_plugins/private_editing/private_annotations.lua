return {
    {
        "danymat/neogen",
        dependencies = "nvim-treesitter/nvim-treesitter",
        opts = {
            languages = {
                python = { template = { annotation_convention = "numpydoc" } },
            },
            snippet_engine = "luasnip",
        },
        cmd = "Neogen",
        keys = {
            {
                "<leader>nn",
                "<cmd>Neogen<cr>",
                desc = "Automatic annotation (Neogen)",
            },
        },
    },
}
