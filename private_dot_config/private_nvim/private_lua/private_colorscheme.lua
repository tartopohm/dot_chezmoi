#!/usr/bin/env lua

-- lighter colours
vim.opt.background = "dark"

-- everforest parametrisation and loading
vim.g.everforest_background = "hard"
vim.g.everforest_enable_italic = 1
vim.g.everforest_better_performance = 1
vim.g.everforest_transparent_background = 2
vim.g.everforest_sign_column_background = "none"
vim.cmd.colorscheme("everforest")

-- Configuration for illuminate
vim.cmd([[hi link IlluminatedWordRead Visual]])
vim.cmd([[hi link IlluminatedWordText Visual]])
vim.cmd([[hi link IlluminatedWordWrite Visual]])
-- Configuration for hlargs
-- vim.cmd [[hi link Hlargs Visual]]
