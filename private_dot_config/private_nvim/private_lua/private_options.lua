local opt = vim.opt

opt.autoindent = true

opt.wrap = true
opt.linebreak = true

opt.number = true -- line numbers
opt.ruler = true -- display line,column
opt.showmode = true -- display mode
opt.signcolumn = "auto:2"

opt.ignorecase = true -- ignore case for search...
opt.smartcase = true -- ... except if it doesn't contain only lowercase letters

opt.tabstop = 4 -- number of spaces for a tab
opt.softtabstop = 4 -- number of spaces inserted with a tab
opt.shiftwidth = 4 -- recommended when tabstop is modified
opt.expandtab = true -- recommended when tabstop is modified

opt.mouse = "a" -- mouse disabled by leaving turning 'a' into a blank

opt.showtabline = 1 -- on when more than one window
opt.splitbelow = true -- hsplit put new window below
opt.splitright = true -- vsplut put new window on the right

opt.confirm = true -- ask to confirm on exit, etc.

opt.wildmode = { "longest", "list" }
opt.wildmenu = true

-- set leader key
vim.g.mapleader = "ù"
vim.g.maplocalleader = "ù"

-- keep current line concealed in normal and command modes
-- opt.concealcursor = "nc"

-- compare to 1 as 0 evaluate to true in lua
if vim.fn.has("termguicolors") == 1 then
    opt.termguicolors = true
end

opt.scrolloff = 8
opt.colorcolumn = "80,100,120"

vim.diagnostic.config({
    underline = false,
    virtual_text = {
        source = "if_many",
    },
    float = {
        source = "always",
    },
})

local disabled_built_ins = {
    "fzf",
    "gzip",
    -- "netrwPlugin",
    -- "netrwSettings",
    -- "netrwFileHandlers",
    "tarPlugin",
    "zipPlugin",
}

for _, plugin in pairs(disabled_built_ins) do
    vim.g["loaded_" .. plugin] = 1
end
