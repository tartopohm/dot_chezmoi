vim.b.indent_blankline_context_patterns =
    { "class_definition", "function_definition", "if_statement", "for_statement" }
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
-- vim.opt.foldtext = "v:lua.vim.treesitter.foldtext()"  from v0.9.5
vim.treesitter.query.set(
    "python",
    "folds",
    [[
        (function_definition
            body: (block)  @fold
        )
        (function_definition
            body: (block
                (expression_statement (string))? @fold
            )
        )
        (class_definition
            body: (block) @fold
        )
        (class_definition
            body: (block
                (expression_statement (string))? @fold
            )
        )
        (expression_statement
            (assignment
                right: [(list) (tuple) (dictionary)]
            ) @fold
        )
        (expression_statement
            (assignment
                right: [
                    (parenthesized_expression)
                    (binary_operator left: (parenthesized_expression))
                ]
            )
        ) @fold
    ]]
)
