#!/usr/bin/env lua

vim.g.tex_flavor = "latex"

vim.opt_local.spell = true
vim.opt_local.spelllang = "en_nz"
vim.keymap.set("i", "<c-s>", "<c-g>u<Esc>[s1z=`]a<c-g>u")

require("cmp").setup.buffer({
    formatting = {
        format = function(entry, vim_item)
            vim_item.menu = ({
                omni = (vim.inspect(vim_item.menu):gsub('%"', "")),
                buffer = "[Buffer]",
                -- formatting for other sources
            })[entry.source.name]
            return vim_item
        end,
    },
    sources = {
        { name = "omni", keyword_length = 2 },
        { name = "path", keyword_length = 2 },
        { name = "buffer", keyword_length = 3 },
        { name = "luasnip" },
    },
})
